class CompaniesController < ApplicationController
  require 'curb'
  before_action :find_company, only: [:show, :edit]

  def new
    @company = Company.new
  end

  def create
    @company = Company.new(company_params)
    @company.user_id = current_user.id
    if @company.save
      redirect_to @company
    else
      render 'new'
    end
  end

  def gen_companies
    count = 0
    1000.times {
      randomstring = SecureRandom.uuid
      x = Curl::Easy.perform("https://www.google.com/search?q=#{randomstring}")
      c = Company.new(name:"test company #{randomstring}", industry: "industry #{randomstring.split('')*1000}", user_id: 1)
      c.save
      count += 1
      p c.name
    }
    redirect_to '/done'
  end

  def edit
  end

  def show
    @company = Company.find(params[:id])
  end

  def index
    @companies = Company.where(user_id: current_user.id)
  end

  private

  def company_params
    params.require(:company).permit(:name, :industry)
  end

  def find_company
    @company = Company.find(params[:id])
  end

end
