class EmployeesController < ApplicationController
  def new
    @company = Company.find(params[:company_id])
    @employee = Employee.new
    @depts = Department.where(company_id: @company.id)
    respond_to do |f|
      f.js
      f.html { p 'Hello'; render 'new' }
    end
  end

  def create
    @company = Company.find(params[:company_id])
    @employee = Employee.new(employee_params)
    @employee.company_id = @company.id
    @employee.generate_eid
    if @employee.save
      msg = "New Employee created!"
      redirect_to company_path(@employee.company_id)
    else
      render 'new'
    end
  end


  def edit
  end

  def index
  end

  def show
  end

  def destroy
    @employee = Employee.find(params[:employee_id])
    company = Company.find(@employee.company_id)
    p "Terminating #{@employee.full_name} from #{company.name}"
    @employee.destroy
    respond_to do |format|
      format.js
      format.html { p 'html response'; redirect_to root_path }
    end
  end

  private

  def employee_params
    params.require(:employee).permit(:first_name, :last_name, :email, :department_id)
  end
end
